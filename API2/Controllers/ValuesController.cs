﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API2.Models;


namespace API2.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Blog")]
    public class BlogController : ApiController
    {
        // GET api/values
        public IEnumerable<Item> Get()
        {
            List<Item> list = new List<Item>();
            list.Add(new Item() { Title = "International Space Station", Text = " International Space Station A rearward view of the International Space Station backdropped by the limb of the Earth. In view are the station's four large, gold-coloured solar array wings, two on either side of the station, mounted to a central truss structure. Further along the truss are six large, white radiators, three next to each pair of arrays. In between the solar arrays and radiators is a cluster of pressurised modules arranged in an elongated T shape, also attached to the truss. A set of blue solar arrays are mounted to the module at the aft end of the cluster. The International Space Station on 23 May 2010 as seen from the departing Space Shuttle Atlantis during STS-132. ISS insignia.svg Station statistics SATCAT no. 25544 Call sign Alpha, Station Crew Fully crewed: 6 Currently aboard: 6 (Expedition 56) Launch 20 November 1998 Launch pad Baikonur 1/5 and 81/23 Kennedy LC-39 Mass ≈ 419,725 kg (925,335 lb)[1] Length 72.8 m (239 ft) Width 108.5 m (356 ft) Height ≈ 20 m (66 ft) nadir–zenith, arrays forward–aft (27 November 2009)[needs update] Pressurised volume 931.57 m3 (32,898 cu ft)[2] (28 May 2016) Atmospheric pressure 101.3 kPa (29.9 inHg; 1.0 atm) Perigee 403 km (250 mi) AMSL[3] Apogee 406 km (252 mi) AMSL[3] Orbital inclination 51.64 degrees[3] Orbital speed 7.67 km/s[3] (27,600 km/h; 17,200 mph) Orbital period 92.49 minutes[3] Orbits per day 15.54[3] Orbit epoch 12 April 2018, 13:09:59 UTC[3] Days in orbit 19 years, 10 months, 4 days (24 September 2018) Days occupied 17 years, 10 months, 22 days (24 September 2018) No. of orbits 102,491 as of July 2017[3] Orbital decay 2 km/month Statistics as of 9 March 2011 (unless noted otherwise) References: [1][3][4][5][6][7] Configuration The components of the ISS in an exploded diagram, with modules on-orbit highlighted in orange, and those still awaiting launch in blue or pink Station elements as of June 2017 (exploded view) The International Space Station (ISS) is a space station, or a habitable artificial satellite, in low Earth orbit. Its first component launched into orbit in 1998, the last pressurised module was fitted in 2011, and the station is expected to operate until 2028. Development and assembly of the station continues, with components scheduled for launch in 2018 and 2019. The ISS is the largest human-made body in low Earth orbit and can often be seen with the naked eye from Earth.[8][9] The ISS consists of pressurised modules, external trusses, solar arrays, and other components. ISS components have been launched by Russian Proton and Soyuz rockets, and American Space Shuttles",
            Likes = 31, Dislikes = 16, IsDirty = false});

            list.Add(new Item()
            {
                Title = "Golden Eagle",
                Text = "The golden eagle (Aquila chrysaetos) is one of the best-known birds of prey in the Northern Hemisphere. It is the most widely distributed species of eagle. Like all eagles, it belongs to the family Accipitridae. These birds are dark brown, with lighter golden-brown plumage on their napes. Immature eagles of this species typically have white on the tail and often have white markings on the wings. Golden eagles use their agility and speed combined with powerful feet and massive, sharp talons to snatch up a variety of prey, mainly hares, rabbits, marmots and other ground squirrels.[2]Golden eagles maintain home ranges or territories that may be as large as 200 km2(77 sq mi).They build large nests in cliffs and other high places to which they may return for several breeding years.Most breeding activities take place in the spring; they are monogamous and may remain together for several years or possibly for life.Females lay up to four eggs, and then incubate them for six weeks. Typically, one or two young survive to fledge in about three months.These juvenile golden eagles usually attain full independence in the fall, after which they wander widely until establishing a territory for themselves in four to five years.",
                Likes = 101,
                Dislikes = 4,
                IsDirty = false
            });

            list.Add(new Item()
            {
                Title = "Article3",
                Text = "T3",
                Likes = 101,
                Dislikes = 4,
                IsDirty = false
            });
            return list ;
        }

        
        [HttpGet]
        [Route("Pagination")]
        public IEnumerable<Item> Get1(int page, int NrPerPage)
        {
            List<Item> list = new List<Item>();
            list.Add(new Item()
            {
                Title = "International Space Station",
                Text = " International Space Station A rearward view of the International Space Station backdropped by the limb of the Earth. In view are the station's four large, gold-coloured solar array wings, two on either side of the station, mounted to a central truss structure. Further along the truss are six large, white radiators, three next to each pair of arrays. In between the solar arrays and radiators is a cluster of pressurised modules arranged in an elongated T shape, also attached to the truss. A set of blue solar arrays are mounted to the module at the aft end of the cluster. The International Space Station on 23 May 2010 as seen from the departing Space Shuttle Atlantis during STS-132. ISS insignia.svg Station statistics SATCAT no. 25544 Call sign Alpha, Station Crew Fully crewed: 6 Currently aboard: 6 (Expedition 56) Launch 20 November 1998 Launch pad Baikonur 1/5 and 81/23 Kennedy LC-39 Mass ≈ 419,725 kg (925,335 lb)[1] Length 72.8 m (239 ft) Width 108.5 m (356 ft) Height ≈ 20 m (66 ft) nadir–zenith, arrays forward–aft (27 November 2009)[needs update] Pressurised volume 931.57 m3 (32,898 cu ft)[2] (28 May 2016) Atmospheric pressure 101.3 kPa (29.9 inHg; 1.0 atm) Perigee 403 km (250 mi) AMSL[3] Apogee 406 km (252 mi) AMSL[3] Orbital inclination 51.64 degrees[3] Orbital speed 7.67 km/s[3] (27,600 km/h; 17,200 mph) Orbital period 92.49 minutes[3] Orbits per day 15.54[3] Orbit epoch 12 April 2018, 13:09:59 UTC[3] Days in orbit 19 years, 10 months, 4 days (24 September 2018) Days occupied 17 years, 10 months, 22 days (24 September 2018) No. of orbits 102,491 as of July 2017[3] Orbital decay 2 km/month Statistics as of 9 March 2011 (unless noted otherwise) References: [1][3][4][5][6][7] Configuration The components of the ISS in an exploded diagram, with modules on-orbit highlighted in orange, and those still awaiting launch in blue or pink Station elements as of June 2017 (exploded view) The International Space Station (ISS) is a space station, or a habitable artificial satellite, in low Earth orbit. Its first component launched into orbit in 1998, the last pressurised module was fitted in 2011, and the station is expected to operate until 2028. Development and assembly of the station continues, with components scheduled for launch in 2018 and 2019. The ISS is the largest human-made body in low Earth orbit and can often be seen with the naked eye from Earth.[8][9] The ISS consists of pressurised modules, external trusses, solar arrays, and other components. ISS components have been launched by Russian Proton and Soyuz rockets, and American Space Shuttles",
                Likes = 31,
                Dislikes = 16,
                IsDirty = false
            });
            list.Add(new Item()
            {
                Title = "Golden Eagle",
                Text = "The golden eagle (Aquila chrysaetos) is one of the best-known birds of prey in the Northern Hemisphere. It is the most widely distributed species of eagle. Like all eagles, it belongs to the family Accipitridae. These birds are dark brown, with lighter golden-brown plumage on their napes. Immature eagles of this species typically have white on the tail and often have white markings on the wings. Golden eagles use their agility and speed combined with powerful feet and massive, sharp talons to snatch up a variety of prey, mainly hares, rabbits, marmots and other ground squirrels.[2]Golden eagles maintain home ranges or territories that may be as large as 200 km2(77 sq mi).They build large nests in cliffs and other high places to which they may return for several breeding years.Most breeding activities take place in the spring; they are monogamous and may remain together for several years or possibly for life.Females lay up to four eggs, and then incubate them for six weeks. Typically, one or two young survive to fledge in about three months.These juvenile golden eagles usually attain full independence in the fall, after which they wander widely until establishing a territory for themselves in four to five years.",
                Likes = 101,
                Dislikes = 4,
                IsDirty = false
            });

            list.Add(new Item()
            {
                Title = "Article3",
                Text = "T3",
                Likes = 101,
                Dislikes = 4,
                IsDirty = false
            });



            return list.Skip((page-1)*NrPerPage).Take(NrPerPage);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
