﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace API2.Models
{
    public class Item
    {
        public string Title { get; set; }
        public string Text { get; set; }

        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public bool IsDirty { get; set; }
    }
}